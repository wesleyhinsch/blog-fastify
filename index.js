// Require the framework and instantiate it
const app = require('fastify')({
    logger: true
})

// Use Fastify Env plugin: https://github.com/fastify/fastify-env
const fastifyEnv = require('fastify-env') // load plugin


app.register(require('fastify-rabbit'), {
    url: 'amqp://localhost'
})

// Import "mongoose"
const mongoose = require("mongoose")
// Import our "User" model
const User = require("./User")

const mongoUrl = process.env.MONGODB_URI || "mongodb://localhost:27017/admin"

const PORT = 3000

/** connect to MongoDB datastore */
try {
    //mongoose.connect(mongoUrl,{ useNewUrlParser: true })
    mongoose.connect(mongoUrl, {
        "auth": { "authSource": "admin" },
        "user": "root",
        "pass": "rootpassword",
    });
} catch (error) {
    console.error(error)
}


// Register the plugin before your routes
app.register(require('fastify-swagger'), {
    exposeRoute: true,
    routePrefix: '/documentation',
    swagger: {
        info: { title: 'blogs-and-users' },
        // Add more options to get a nicer page ✨
    },
})

const options = {
    confKey: 'config', // optional, default: 'config'
    schema: {
        type: 'object',
        required: ['PORT'],
        properties: {
            PORT: {
                type: 'number',
                default: 1000
            }
        }
    }
}

app
    .register(fastifyEnv, options)
    .ready((err) => {
        if (err) console.error(err)

        //console.log(PORT)
        // output: { PORT: 1000 }
    })

// hooks
app.addHook('onRoute', (routeOptions) => {
    console.log(`Registered route: ${routeOptions.url}`)
})


// Declare a route
app.get('/', function (req, reply) {
    reply.send({ hello: 'world' })
})


app.get("/api/users", (request, reply) => {
    User.find({}, (err, users) => {
        if (!err) {
            reply.send(users)
        } else {
            reply.send({ error: err })
        }
    })
})
app.get("/api/users/:userId", (request, reply) => {
    var userId = request.params.userId
    User.findById(userId, (err, user) => {
        if (!err) {
            reply.send(user)
        } else {
            reply.send({ error: err })
        }
    })
})
app.post("/api/users", (request, reply) => {
    var user = request.body
    User.create(user, (err, user) => {
        if (!err) {
            reply.send(user)
        } else {
            reply.send({ error: err })
        }
    })
})
app.put("/api/users/:userId", (request, reply) => {
    var userId = request.params.userId
    User.findById(userId, (err, user) => {
        if (!err) {
            user.remove((er) => {
                if (!er) {
                    reply.send("USER DELETED")
                } else {
                    reply.send({ error: er })
                }
            })
        } else {
            reply.send({ error: err })
        }
    })
})


// Publish message to queue
app.get('/amqp', async function (req, reply) {
    const channel = this.rabbit.channel

    // Create queue if it does not exist
    const ok = await channel.assertQueue('hello')
    if (!ok) {
        // handle failed to assert queue
    }

    // Push message to queue
    try {
        await channel.sendToQueue('hello', Buffer.from('The is my message to you...'))
        reply.send('Message sent!')
    } catch (err) {
        reply.log.error(err)
    }

})

// Consume messages from queue
app.register(async function(fastify, opts) {
    const channel = fastify.rabbit.channel

    // Create queue if it does not exist
    const ok = await channel.assertQueue('hello')
    if (ok) {
        // start consuming queue
        channel.consume('hello', msg => {
            fastify.log.info(msg.content.toString())
            channel.ack(msg)
        })
    }
})

// Register routes to handle blog posts
const blogRoutes = require('./routes/blogs')
blogRoutes.forEach((route, index) => {
    app.route(route)
})

// Run the server!
app.listen(PORT, (err, address) => {
    if (err) {
        app.log.error(err)
        process.exit(1)
    }
    app.log.info(`server listening on ${address}`)
})
