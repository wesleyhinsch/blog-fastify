const mongoose = require('mongoose')
let BlogSchema = new mongoose.Schema({
    id: Number,
    title: String
})
module.exports = mongoose.model('Blog', BlogSchema)